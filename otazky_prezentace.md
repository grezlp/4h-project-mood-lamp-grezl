# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 6+ hodin                        |
| odkud jsem čerpal inspiraci                   | https://www.instructables.com/RGB-Lamp-WiFi/ |
| odkaz na video                                | *URL video*                     |
| jak se mi to podařilo rozplánovat             | celkem chvalitebně mínus        |
| proč jsem zvolil tento design                 | líbí se mi :D                   |
| zapojení                                      | ![My Image](/dokumentace/schema/návrh_mood_lamp.jpg)         |
| z jakých součástí se zapojení skládá          | LED páska, měřič teploty a vlhkosti, WEMOS   |
| realizace                                     | ![My Image2](/dokumentace/fotky/mood_lamp_done.jpeg) |
| UI                                            | ![My Image3](/dokumentace/fotky/UI.PNG)               |
| co se mi povedlo                              | celkový vzhled, sehnání informací            |
| co se mi nepovedlo/příště bych udělal/a jinak | návrh od začátku sám            |
| zhodnocení celé tvorby (návrh známky)         | 1                               |